﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Task3 : MonoBehaviour
{
    // Start is called before the first frame update
    public Text task3;
    int i, j, k = 0, kk = 1;
    void Start()
    {
        for (i = 1; i <= 5; i++)
        {
            k++;
            if (k % 2 == 1)
            {
                kk = 1;
                for (j = 1; j < i + 1; j++)
                {
                    if (kk == 1)
                        task3.text += (1 + " ");
                    else
                        task3.text += (0 + " ");
                    kk = kk * (-1);
                }
            }
            else
            {
                kk = -1;
                for (j = 1; j < i + 1; j++)
                {
                    if (kk == 1)
                        task3.text += (1 + " ");
                    else
                        task3.text += (0 + " ");
                    kk = kk * (-1);
                }
            }
            task3.text += ("\n");
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
