﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Task6 : MonoBehaviour
{
    bool[] array1 = new bool[] {false, false, false, false, false, false, false, false, false, false};
    bool[] array2 = new bool[] { false, false, false, false, false, false, false, false, false, false };
    int num1= 825482, num2 = 908762;
    int ost;
    public Text task6; 
    void Start()
    {
        while (num1>0)
        {
            ost = num1 % 10;
            array1[ost]=true;
            num1 = num1 / 10;
        }
        while (num2 > 0)
        {
            ost = num2 % 10;
            array2[ost] = true;
            num2 = num2 / 10;
        }
        for (int i=0; i<10;i++)
        {
            if (array1[i] == array2[i]&&array1[i])
                task6.text += i;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
