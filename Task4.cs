﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Task4 : MonoBehaviour
{
    // Start is called before the first frame update
    int i;
    public Text score;
    void Start()
    {
        i = UnityEngine.Random.Range(60, 101);
        if (i < 70)
            score.text = "За "+i+" баллов вы получили удовлетворительно";
        if ((i >= 70) && (i < 80))
            score.text = "За "+i+" баллов вы получили хорошо";
        if (i >= 80)
            score.text = "За "+i+" баллов вы получили отлично";
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
