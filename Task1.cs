﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Task1 : MonoBehaviour
{

    int sum = 0;
    bool test = true;
    // Start is called before the first frame update
    void Start()
    {
        
        for (int i = 1; i<13; i++)
        {
            test = true;
            for (int j = 1; j<=i; j++)
            {
                if (i % j == 0 && j != 1 && j != i)
                    test = false;
            }
            if (test)
                sum += i;
        }
        Debug.Log(sum);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
