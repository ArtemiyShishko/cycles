﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Task7 : MonoBehaviour
{
    // Start is called before the first frame update
    int i;
    int[] arr = new int[10];
    public Text task7;
    void Start()
    {
        for (int num = 40; num<=50; num++)
        {
            i = num;
            while (i>1)
            {
                if (i % 2 == 0)
                    i = i / 2;
                else
                    i = (i * 3 + 1) / 2;
            }
            task7.text += i;
        }
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
